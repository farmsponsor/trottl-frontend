import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import Vuex from 'vuex'

//Import Global Component(s)
import DashboardLayout from '@/layouts/dashboard/Index'
import DashboardBtn from '@/components/base/DashboardBtn'
import BackBtn from '@/components/base/BackBtn'
// import ProjectCard from '@/components/dashboard/ProjectCard'

 // import drizzleOptions and drizzleVuePlugin
// import drizzleVuePlugin from '@drizzle/vue-plugin'
// import drizzleOptions from './drizzleOptions'

//Register Global Component(s)
Vue.component('DashboardLayout', DashboardLayout)
Vue.component('DashboardBtn', DashboardBtn)
Vue.component('BackBtn', BackBtn)
// Vue.component('ProjectCard', ProjectCard)

// Use Vuex
Vue.use(Vuex)

// Create and configure your Vuex Store
// const store = new Vuex.Store({ state: {} })
 
// Register the drizzleVuePlugin
// Vue.use(drizzleVuePlugin, { store, drizzleOptions })

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
