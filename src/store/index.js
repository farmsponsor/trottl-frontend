import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    articles: require('@/data/articles.json'),
    drawer: false,
    generalNavLinks: [
      {
        name: "Whitepaper",
        text: 'Whitepaper',
        slug: '#',
      }
    ],
    dashboardNavLinks: [
      {
        name: "Wallet",
        text: 'Wallet',
        slug: '/wallet',
        icon: require('@/assets/dashboard/wallet-icon.svg')
      },
      {
        name: "Dashboard",
        text: 'Dashboard',
        slug: '/dashoard',
        icon: require('@/assets/dashboard/dashboard-icon.svg')
      }
    ],
    accounts: [],
    activeAccount: null,
    trottlToken: {},
    trottlTimelock: {},
    trottlGovernor: {},
    web3: {},
    balance: null,
    netId: "",
    isMetaMaskInstalled: false,
    isUserConnected: false
  },
  getters: {
    categories: state => {
      const categories = []

      for (const article of state.articles) {
        if (
          !article.category ||
          categories.find(category => category.text === article.category)
        ) continue

        const text = article.category

        categories.push({
          text,
          href: '#!',
        })
      }

      return categories.sort().slice(0, 4)
    },
    links: (state, getters) => {
      return state.generalNavLinks
      // return state.generalNavLinks.concat(getters.categories)
    },
    metaMaskInstallStatus: state => {
      return state.isMetaMaskInstalled
    },
    getActiveAccount: state => {
      return state.activeAccount
    },
    userConnected: state => {
      return state.isUserConnected
    }
  },
  mutations: {
    setDrawer: (state, payload) => (state.drawer = payload),
    toggleDrawer: state => (state.drawer = !state.drawer),
    setMetaMaskInstallStatus: state => (state.isMetaMaskInstalled = true),
    setUser: (state) => {
      state.isUserConnected = true
    },
    setAccounts: (state, accounts) => (state.accounts = accounts),
    setActiveAccount: (state, accounts) =>{
      state.activeAccount = accounts[0]
    },
    setState: (state, payload) => {
      state.web3 = payload.web3,
      state.balance = payload.balance
      state.netId = payload.netId
    },
    setContracts: (state, payload) => {
      state.trottlToken = payload.trottl
      // state.trottlTimelock = payload.trottlTimelock
      // state.trottlGovernor = payload.trottlGovernor
    }
  },
  actions: {
    setMetaMaskInstallStatusToTrue({ commit }) {
      commit('setMetaMaskInstallStatus')
    },
    loadAccounts({ commit, state }, accounts) {
      return new Promise((resolve, reject) => {
        commit('setAccounts', accounts)
        commit('setActiveAccount', accounts)
        resolve(state.accounts)
      })
    },
    setAppState({ commit, state }, data) {
      return new Promise((resolve, reject) => {
        commit('setState', data)
        resolve(state)
      })
    },
     setContractState({ commit, state }, data) {
      return new Promise((resolve, reject) => {
        commit('setContracts', data)
        resolve(state)
      })
    },
    connectUser({ commit }) {
      // connect to metamask or ethereum provider
      // get active account address
      // load blockchain data
      // set isUserConnected to true
      // redirect to dashboard
      
      // commit('setActiveAccount')
      commit('setUser')
    }
  },
})

