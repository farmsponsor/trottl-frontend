import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'
import {mapGetters} from 'vuex'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import(/* webpackChunkName: "Home"*/ '@/views/Home.vue')
  },
  {
    path: '/earn',
    name: 'Earn',
    component: () => import(/* webpackChunkName: "Earn" */ '../views/dashboard/Earn.vue'),
    meta: {requiresAuth: true}
  },
   {
    path: '/dashboard',
    name: 'Dashboard',
    component: () => import(/* webpackChunkName: "Dashboard" */ '../views/dashboard/Index.vue'),
    meta: {requiresAuth: true}
  },
  {
    path: '/welcome',
    name: 'Welcome',
    component: () => import(/* webpackChunkName: "Welcome" */ '../views/dashboard/Welcome.vue'),
    meta: {requiresAuth: true}
  }
  ,
  {
    path: '/wallet',
    name: 'Wallet',
    component: () => import(/* webpackChunkName: "Wallet" */ '../views/dashboard/Wallet.vue'),
      meta: {
      requiresAuth: true
    }
  }
  //,
  //   {
  //   name: "NotFound",
  //   path: "/404",
  //   alias: "*",
  //   component: () => import(/*WebpackChunckName: "NotFound" */ "../views/NotFound")
  // }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
   scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    }else {
      const position = {};
      if (to.hash) {
        position.selector = to.hash;
        // if(to.hash === "#experience"){
        //   position.offset = {y:140};
        // }
        if(document.querySelector(to.hash)){
          return position;
        }
        return false
      }
    }
  },
  routes
})

router.beforeEach((to, from, next)=> {
  // check if page requires authentication
  if(to.matched.some(record => record.meta.requiresAuth)){
    if(!store.getters.userConnected){
      next({
        name: "Home",
        query: {redirect: from.fullPath}
      });
    }else {
      next();
    }
  } else {
    next();
  }
})

export default router
