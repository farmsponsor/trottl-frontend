import TrottlToken from './contracts/TrottlToken.json'
import TrottlTimeLock from './contracts/TrottlTimeLock.json'
// import TrottlGovernorAlpha from './contracts/TrottlGovernorAlpha.json'

const options = {
  web3: {
    block: false,
    fallback: {
      type: 'ws',
      url: 'ws://127.0.0.1:7545'
    }
  },
  contracts: [
    TrottlToken,
    TrottlTimeLock
    // TrottlGovernorAlpha
  ],
  polls: {
    accounts: 15000
  }
}

export default options